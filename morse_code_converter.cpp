#include <iostream>
#include <string>
#include <map>
#include <ctype.h>
#include <vector>

std::map<char, std::string> morse_code_map = {
    {'A', ".-"}, 
    {'B', "-..."}, 
    {'C', "-.-."}, 
    {'D', "-.."}, 
    {'E', "."}, 
    {'F', "..-."},
    {'G', "--."}, 
    {'H', "...."}, 
    {'I', ".."}, 
    {'J', ".---"}, 
    {'K', "-.-"}, 
    {'L', ".-.."},
    {'M', "--"}, 
    {'N', "-."}, 
    {'O', "---"}, 
    {'P', ".--."},
    {'Q', "--.-"}, 
    {'R', ".-."}, 
    {'S', "..."}, 
    {'T', "-"}, 
    {'U', "..-"}, 
    {'V', "...-"},
    {'W', ".--"}, 
    {'X', "-..-"}, 
    {'Y', "-.--"}, 
    {'Z', "--.."},
    {' ', "     "},
    {'1', ".----"}, 
    {'2', "..---"}, 
    {'3', "...--"}, 
    {'4', "....-"}, 
    {'5', "....."},
    {'6', "-...."}, 
    {'7', "--..."}, 
    {'8', "---.."}, 
    {'9', "----."},
    {'0', "-----"},
    {'.', ".-.-.-"}, 
    {',', "--..--"}, 
    {'?', "..--.."}, 
    {'\'', ".----."}, 
    {'!', "-.-.--"},
    {'/', "-..-."}, 
    {':', "---..."}, 
    {';', "-.-.-."}, 
    {'=', "-...-"},
    {'+', ".-.-."}, 
    {'-', "-....-"}, 
    {'_', "..--.-"}, 
    {'"', ".-..-."},
    {'@', ".--.-."}
};

    
void show_usage(const std::string& name) {
    std::cerr << "Usage: " << name << " <term>\n"
              << "Options: \n"
              << "\t-h, --help             | Shows this help message\n"
              << "\t-e, --encode           | Encodes text to morse code\n"
              << "\t-d, --decode           | Decodes morse code to text"
              << std::endl;
}

char find_key_to_value(std::string s) {
    for (auto i = morse_code_map.begin(); i != morse_code_map.end(); i++) {
        if (i->second == s) {
            return i->first;
        }
    }
    return ' ';
}

std::string encode_to_morse(const std::string& term) {
    std::string encoded_term = "";
    
    // Loop through term and find translatation for character in map
    for (char c : term) {
        encoded_term += morse_code_map.find(toupper(c))->second + " ";
    }
    
    return encoded_term;
}

std::string decode_to_text(const std::string& term) {
    std::string decoded_term = "";
    std::string morse_letter = "";
    
    // Loop through sequence and find translatations for each morse character in map
    for (std::size_t i = 0; i < term.length(); i++) {
        // If the term isn't ' ' the letter isn't finished yet
        if (term[i] != ' ') {
            morse_letter += term[i];
        // The letter is finished, but it could be also the end of the word - so we check if the next character is also ' '
        } else if (term[i] == ' ' && term[i + 1] == ' ') {
            // Add decoded letter to term
            decoded_term += find_key_to_value(morse_letter);
            // Add space for next word
            decoded_term += " ";
            // Empty morse letter to get ready for the next
            morse_letter = "";
            // Jump 5 characters ahead, because in morse the space between words should be 5 units
            i += 5;
        // The letter is finished, but not the word
        } else {
            // Add decoded letter to term
            decoded_term += find_key_to_value(morse_letter);
            // Empty morse letter to get ready for the next
            morse_letter = "";
        }
    }
    
    return decoded_term;
}

// False "decode_or_encode" is only decoding, true is encoding
void display_result(const std::string& term, bool decode_or_encode) {
    std::string result;
    
    if (decode_or_encode) {
        result = encode_to_morse(term);
    } else {
        result = decode_to_text(term);
    }
    
    std::cout << result << std::endl;
}

void parse_arguments(int argc, char* argv[]) {
    const int MIN_ARGUMENT_COUNT = 2;
    const int MAX_ARGUMENT_COUNT = 3;
    
    std::string term;
    
    if (argc >= MIN_ARGUMENT_COUNT && argc <= MAX_ARGUMENT_COUNT) {
        // Called with no arguments
        if (argc == 2) {
            if (std::string(argv[1]) == "-d" || std::string(argv[1]) == "--decode") {
                std::cin >> term;
                display_result(term, false);
            } else if (std::string(argv[1]) == "-e" || std::string(argv[1]) == "--encode") {
                std::cin >> term;
                display_result(term, true);
            } else {
                show_usage(argv[0]);
            }
            
        } else {
            // Loop through arguments skipping argv[0] by starting at "i = 1", because this is the name with which the program was called.
            for (std::size_t i = 1; i < argc; i++) {
                if (std::string(argv[i]) == "-h" || std::string(argv[i]) == "--help") {
                    show_usage(argv[0]);
                    break;
                } else if (std::string(argv[i]) == "-d" || std::string(argv[i]) == "--decode") {
                    term = argv[i + 1];
                    display_result(term, false);
                } else if (std::string(argv[i]) == "-e" || std::string(argv[i]) == "--encode") {
                    term = argv[i + 1];
                    display_result(term, true);
                }
            }
        }
    } else {
        show_usage(argv[0]);
    }
}

int main(int argc, char* argv[]) {
    parse_arguments(argc, argv);
    return 0;
}
